package ru.t1.godyna.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.godyna.tm.dto.model.UserDTO;

@Repository
@Scope("prototype")
public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(String login);

    @Nullable
    UserDTO findByEmail(String email);

}
