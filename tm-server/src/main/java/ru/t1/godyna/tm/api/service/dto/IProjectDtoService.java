package ru.t1.godyna.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.model.ProjectDTO;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface IProjectDtoService {

    @NotNull
    ProjectDTO add(@Nullable ProjectDTO model);

    @NotNull
    ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO model);

    @NotNull
    Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> models);

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<ProjectDTO> findAll();

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);

    @NotNull
    ProjectDTO remove(@Nullable ProjectDTO model);

    @NotNull
    ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO model);

    void removeAll(@Nullable Collection<ProjectDTO> collection);

    @NotNull
    ProjectDTO removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> models);

    @NotNull
    ProjectDTO update(@NotNull ProjectDTO model);

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}
